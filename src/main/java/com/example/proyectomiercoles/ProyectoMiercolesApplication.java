package com.example.proyectomiercoles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoMiercolesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProyectoMiercolesApplication.class, args);
    }

}
