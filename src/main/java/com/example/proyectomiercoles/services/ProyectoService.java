package com.example.proyectomiercoles.services;

import com.example.proyectomiercoles.models.Lenguaje;
import com.example.proyectomiercoles.models.Proyecto;
import com.example.proyectomiercoles.reps.LenguajeRepository;
import com.example.proyectomiercoles.reps.ProyectoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProyectoService {

    @Autowired
    private ProyectoRepository proyectoRep;

    public List<Proyecto> getAllPros() {
        return proyectoRep.findAll();
    }

}
