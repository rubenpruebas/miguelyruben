package com.example.proyectomiercoles.services;

import com.example.proyectomiercoles.models.Lenguaje;
import com.example.proyectomiercoles.reps.LenguajeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LenguajeService {
    @Autowired
    private LenguajeRepository repositorioLenguaje;

    public List<Lenguaje> getAllLenguajes() {
        return repositorioLenguaje.findAll();
    }

    public Lenguaje getLenguajeById(Long id) {
        return repositorioLenguaje.findById(id).orElse(null);
    }

}
