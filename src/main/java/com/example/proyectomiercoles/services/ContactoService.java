package com.example.proyectomiercoles.services;

import com.example.proyectomiercoles.models.Contacto;
import com.example.proyectomiercoles.models.Proyecto;
import com.example.proyectomiercoles.reps.ContactoRepository;
import com.example.proyectomiercoles.reps.ProyectoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactoService {


    @Autowired
    private ContactoRepository contactoRep;

    public List<Contacto> geAllContactos() {
        return contactoRep.findAll();
    }

    public void save(Contacto c) {
        contactoRep.save(c);
    }

}
