package com.example.proyectomiercoles;


import com.example.proyectomiercoles.models.Contacto;
import com.example.proyectomiercoles.models.Lenguaje;
import com.example.proyectomiercoles.models.Proyecto;
import com.example.proyectomiercoles.services.ContactoService;
import com.example.proyectomiercoles.services.LenguajeService;
import com.example.proyectomiercoles.services.ProyectoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.swing.text.TabExpander;
import java.util.List;

@Controller
public class RootController {
    @Autowired
    private LenguajeService lenguajeServicio;
    @Autowired
    private ProyectoService proyectoServicio;
    @Autowired
    private ContactoService contactoServicio;


    @GetMapping("/")
    public String index(Model model) {
        List<Proyecto> pros = proyectoServicio.getAllPros();
        model.addAttribute("proyectos", pros);

        List<Lenguaje> lengs = lenguajeServicio.getAllLenguajes();
        model.addAttribute("lenguajes", lengs);

        return "index";
    }

    @PostMapping("/")
    public String post(@RequestParam String nombre, @RequestParam String email, @RequestParam String telefono, Model model) {
        try {
            Contacto contacto = new Contacto();
            contacto.setNombre(nombre);
            contacto.setTelefono(telefono);
            contacto.setEmail(email);
            contactoServicio.save(contacto);
            return "index";
        } catch (NumberFormatException nfe) {
            model.addAttribute("message", "El salario debe ser un numero");
            return "index";
        }
    }

}