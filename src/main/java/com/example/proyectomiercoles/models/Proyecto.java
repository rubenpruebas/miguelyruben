package com.example.proyectomiercoles.models;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "proyectos")
public class Proyecto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_proyecto;

    private String nombre;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "relacion",
            joinColumns = @JoinColumn(name = "id_proyecto"), inverseJoinColumns = @JoinColumn(name = "id_lenguaje")
    )
    private List<Lenguaje> lenguajesUtilizados = new ArrayList<>();

    public Long getId_proyecto() {
        return id_proyecto;
    }

    public void setId_proyecto(Long id_proyecto) {
        this.id_proyecto = id_proyecto;
    }

    public String getNombre() {
        return nombre;
    }

    public List<Lenguaje> getLenguajesUtilizados() {
        return lenguajesUtilizados;
    }

    public void setLenguajesUtilizados(ArrayList<Lenguaje> lenguajesUtilizados) {
        this.lenguajesUtilizados = lenguajesUtilizados;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
